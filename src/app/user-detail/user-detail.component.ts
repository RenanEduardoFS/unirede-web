import { Component, OnInit } from '@angular/core';
import { User } from '../Model/user';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user.service';
import { FormBuilder, FormGroup, FormControl, Validators } from "@angular/forms";

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  user = new User();
  formUserData: FormGroup;
  constructor(private route: ActivatedRoute, private userService: UserService, private fb: FormBuilder) {
    this.createForms();
   }

  ngOnInit() {
    this.getUser();
  }
  ngAfterViewInit() {
    
  }

  private createForms() {

    this.formUserData = this.fb.group({
      login: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(12)]],
      password: ['', [Validators.required]],
      accessLevel: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(40)]],
   
    });
  }


  getUser() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.userService.getUserById(id).subscribe(res => {
      console.log('getting user...');
      this.user = res;
      console.log(res);
      this.setUser();
    });
  }
  private createUser() {
    if (!this.user) {
      this.user = new User();
    }
  }

  private setUser() {
    if(!this.user)
    {
      this.createUser();
    }
    this.user.login = this.formUserData.get('login').value;
    this.user.password = this.formUserData.get('password').value;
    this.user.accessLevel = this.formUserData.get('accessLevel').value;
  }

}
