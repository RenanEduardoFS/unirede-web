import { Component, OnInit } from '@angular/core';
import { User } from '../Model/user';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  users: User[] = [];
  constructor(private userService: UserService) { }

  ngOnInit() {
    console.log('ngOnInit');
    this.getUserList();
  }

  getUserList() {
    this.userService.getUsers().subscribe(result => {
      console.log('pegando lista')
      this.users = result;
      console.log(this.users)
    })
  }

}
