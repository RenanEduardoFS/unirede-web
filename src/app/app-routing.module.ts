import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserCrudComponent } from './user-crud/user-crud.component';
import { Routes, RouterModule } from "@angular/router";
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserListComponent } from './user-list/user-list.component';

const routes: Routes = [
  {path: '', component:UserListComponent},
  { path: 'userCrud', component: UserCrudComponent },
  { path: 'detail/:id', component: UserDetailComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]

 
})


export class AppRoutingModule { }
