export class User{
    private _id: number;
  public get id(): number {
    return this._id;
  }
  public set id(value: number) {
    this._id = value;
  }
    private _login: string;
    public get login(): string {
        return this._login;
    }
    public set login(value: string) {
        this._login = value;
    }
    private _password: string;
    public get password(): string {
        return this._password;
    }
    public set password(value: string) {
        this._password = value;
    }
    private _accessLevel: string;
    public get accessLevel(): string {
        return this._accessLevel;
    }
    public set accessLevel(value: string) {
        this._accessLevel = value;
    } 
}