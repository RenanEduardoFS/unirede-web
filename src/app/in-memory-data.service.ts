import { InMemoryDbService } from 'angular-in-memory-web-api';
import { User } from './Model/user';


export class InMemoryDataService implements InMemoryDbService {
    createDb() {
      const users = [
        { id:1, login: 'john', password: '123456', accessLevel: 'interno' },
        { id:2, login: 'jane', password: 'myCat123', accessLevel: 'cliente'},
        { id:3, login: 'steve', password: 'football97', accessLevel: 'administrador' }
      ];
      return {users};
    }

    genId(users: User[]){
      return users.length > 0 ? Math.max(...users.map(user => user.id)) + 1 : 11;
    }
    
}